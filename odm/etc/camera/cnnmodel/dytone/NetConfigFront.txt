// every line must contain '//' or setting
// whole line is commented if '//' is found
// this file is parsed by 'find' function
// make sure no space is left @ end of file
//
// should set modelFolder - modelName - nnHardWare
//
// modelFolder : folder contains ms file
modelFolder = /odm/etc/camera/cnnmodel/dytone
//
// modelName : ms name
modelName = /dytone_front.om
// input tensor number of network
nnInputNum = 3
//// setting of input1 img
// netInC:1/y;2/yuv422
netInC = 2
netInW = 368
netInH = 270
// downscale
yuv10Bit28BitDivW = 2
yuv10Bit28BitDivH = 2
//// setting of input2 exif, legth of input exif tensor
netInC2 = 2
//// setting of input3 facemask
faceMaskInW = 46
faceMaskInH = 34
// setting of netout
netOutLen = 99
// sigmoidOnC : do sigmoid on C
sigmoidOnC = True
useFaceRect = True
// net out gain and bias setting
gainNoise = 1
biasNoise = 0
gainAlpha = 2
biasAlpha = 0
gainSigma = 1;
biasSigma = 0;
gainNoiseDs16 = 1
biasNoiseDs16 = 0
gainAlphaDs16 = 2
biasAlphaDs16 = 0
gainSigmaDs16 = 1
biasSigmaDs16 = 0
// !! keep this comment line as last line !!